# README #

Nodejs version 10.13.0

## Installation ##

```bash
# if not installed install a global nest cli
$ npm install -g @nestjs/cli

# install node dependencies
$ npm install
```

## Running the app ##

```bash
# development
$ npm start

# watch mode
$ npm run dev

# production mode
$ npm run prod
```

## Test ##

```bash
# unit tests
$ npm test
```

## Utilities Commands ##

```bash
# Generate
$ nest g module moduleName
$ nest g controller controllerName
$ nest g class className
$ nest g service serviceName
```
