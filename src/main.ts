import dotenvFlow = require('dotenv-flow');
dotenvFlow.config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function server() {
  const app = await NestFactory.create(AppModule);
  await app.listen(process.env.PORT);

  console.log(`Server started on ${process.env.PORT}`);
}
server();
