import * as mongoose from 'mongoose';

export const GoalsSchema = new mongoose.Schema({
  name: { type: String },
  code: { type: String },
  points: { type: Number, default: 0 },
  coins: { type: Number, default: 0 },
  groupId: { type: mongoose.Types.ObjectId },
  publishAt: { type: Date },
  expireAt: { type: Date },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: null },
  deletedAt: { type: Date, default: null },
});
