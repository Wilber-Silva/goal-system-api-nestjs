import { Document } from 'mongoose';

export class Goals extends Document {
  name: string;
  code: string;
  points: number;
  coins: number;
  groupId: string;
  publishAt: Date;
  expireAt: Date;
  createdAt: Date = new Date();
  updatedAt?: Date = null;
  deletedAt?: Date = null;
}
