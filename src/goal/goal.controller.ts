import { GoalService } from './goal.service';
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  HttpCode,
  Body,
  Param,
  Query,
  HttpStatus,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { Goals } from './goals';
import { UpdateResponse } from '../http/update.response';
import { CreatedResponse } from '../http/created-response';

@Controller('goal')
export class GoalController {
  constructor(private goalService: GoalService) {}

  @Get(':_id')
  async findById(@Param() id: Types.ObjectId): Promise<Goals> {
    return this.goalService.findById(id);
  }
  @Get()
  async list(@Query() goal: Goals): Promise<Goals[]> {
    return this.goalService.list(goal);
  }

  @Put(':_id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async update(
    @Param() id: Types.ObjectId,
    @Body() goal: Goals,
  ): Promise<UpdateResponse> {
    return this.goalService.update(id, goal);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() goal: Goals): Promise<CreatedResponse> {
    const created = await this.goalService.create(goal);
    return new CreatedResponse(created);
  }

  @Delete()
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete (@Param() id: Types.ObjectId): Promise<void> {
    return this.goalService.delete(id)
  }
}

@Controller('goal/many')
export class GoalManyController {
  constructor(private goalService: GoalService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create (@Body() goals: Goals[]) : Promise<Goals[]> {
    return this.goalService.createMany(goals)
  }

}
