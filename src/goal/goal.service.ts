import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Goals } from './goals';
import { NextGoal } from './next-goal';
import { UpdateResponse } from '../http/update.response';

@Injectable()
export class GoalService {
  constructor(@InjectModel('goals') private readonly goalModel: Model<Goals>) {}

  async duplicate(goal: Goals): Promise<Goals> {
    return this.goalModel.findOne({ ...goal });
  }

  async findById(id: Types.ObjectId): Promise<Goals> {
    return this.goalModel.findOne(id);
  }
  async list(goal: Goals): Promise<Goals[]> {
    return this.goalModel.find({ ...goal });
  }
  async update(id: Types.ObjectId, goal: Goals): Promise<UpdateResponse> {
    goal.updatedAt = new Date();
    return this.goalModel.updateOne(id, { ...goal });
  }
  async create(goal: Goals): Promise<Goals> {
    const exists = await this.duplicate(goal);
    if (exists) return exists;
    return this.goalModel.create(goal);
  }

  async delete (id: Types.ObjectId): Promise<void> {
    return this.goalModel.updateOne(id, {  updatedAt: new Date(), deletedAt: new Date() })
  }

  async createMany (goals: Goals[]): Promise<Goals[]> {
    return this.goalModel.insertMany(goals)
  }
  async next(goal: Goals): Promise<Goals> {
    return this.goalModel.findOne({
      points: { $gt: goal.points },
      groupId: goal.groupId,
      deletedAt: null,
      publishAt: { $lte: new Date() },
      expireAt: { $gte: new Date() },
    });
  }
  async verify(id: Types.ObjectId, points: number): Promise<NextGoal> {
    const myGoal = await this.findById(id);
    if (!myGoal)
      throw new HttpException('Current goal not exists', HttpStatus.NOT_FOUND);
    const nextGoal = await this.next(myGoal);

    const complete = points >= myGoal.points;

    return new NextGoal(
      myGoal._id,
      myGoal.groupId,
      nextGoal ? nextGoal._id : null,
      myGoal.coins,
      complete,
    );
  }
}
