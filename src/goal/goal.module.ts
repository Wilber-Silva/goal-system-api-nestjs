import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GoalController, GoalManyController } from './goal.controller';
import { GoalSystemController } from './goal-system.controller';
import { GoalService } from './goal.service';
import { GoalsSchema } from './goal.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'goals', schema: GoalsSchema }]),
  ],
  controllers: [GoalController, GoalSystemController, GoalManyController],
  providers: [GoalService],
})
export class GoalModule {}
