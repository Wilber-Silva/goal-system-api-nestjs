export class NextGoal {
  currentGoalId: string;
  groupId: string;
  newGoalId?: string;
  coins?: number;
  complete?: boolean;

  constructor(
    currentGoalId: string,
    groupId: string,
    newGoalId?: string,
    coins?: number,
    complete?: boolean,
  ) {
    this.currentGoalId = currentGoalId;
    this.groupId = groupId;
    this.newGoalId = newGoalId;
    this.coins = coins;
    this.complete = complete;
  }
}
