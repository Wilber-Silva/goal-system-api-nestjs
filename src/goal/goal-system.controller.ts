import { GoalService } from './goal.service';
import { Controller, Get, Query, Param } from '@nestjs/common';
import { Types } from 'mongoose';
import { Goals } from './goals';
import { NextGoal } from './next-goal';

@Controller('/next')
export class GoalSystemController {
  constructor(private goalService: GoalService) {}
  @Get()
  async nextGoal(@Query() goal: Goals): Promise<Goals> {
    return this.goalService.next(goal);
  }
  @Get(':_id')
  async verify(
    @Param() id: Types.ObjectId,
    @Query() goal: Goals,
  ): Promise<NextGoal> {
    return this.goalService.verify(id, goal.points);
  }
}
