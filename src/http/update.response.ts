export class UpdateResponse {
  n: number;
  nModified: number;
  ok: number;
}
