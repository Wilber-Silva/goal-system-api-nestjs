import { Document } from 'mongoose';

export class CreatedResponse {
  _id: string;

  constructor(document: Document) {
    this._id = document._id;
  }
}
