import { Controller, Get } from '@nestjs/common';

@Controller('/')
export class AppController {
    @Get()
    home (): string {
        return 'Loyalty - goal-system-api'
    }
}
