import * as mongoose from 'mongoose';

export const GroupSchema = new mongoose.Schema({
  name: { type: String },
  tag: { type: String },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: null },
  deletedAt: { type: Date, default: null },
});
