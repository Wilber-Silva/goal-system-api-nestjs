import { UpdateResponse } from './../http/update.response';
import { CreatedResponse } from './../http/created-response';
import { Types } from 'mongoose';
import { GroupService } from './group.service';
import { Controller, Get, Post, Put, Delete, HttpCode, Body, Query, HttpStatus, Param } from '@nestjs/common';
import { Group } from './group';

@Controller('group')
export class GroupController {
  constructor(private groupService: GroupService) {}

  @Get(':_id')
  async find(@Param() id: Types.ObjectId): Promise<Group> {
    return this.groupService.find(id)
  }

  @Get()
  async list(@Query() group: Group): Promise<Group[]> {
    return this.groupService.list(group);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() group: Group): Promise<CreatedResponse> {
    const created = await this.groupService.create(group);
    return new CreatedResponse(created)
  }

  @Put(':_id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async update (@Param() id: Types.ObjectId, @Body() group: Group): Promise<UpdateResponse> {
    return this.groupService.update(id, group)
  }

  @Delete(':_id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete (@Param() id: Types.ObjectId): Promise<void>{
    return this.groupService.delete(id)
  }
}


@Controller('group/many')
export class GroupManyController {
  constructor(private groupService: GroupService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create (@Body() groups: Group[]): Promise<Group[]>{
    return this.groupService.createMany(groups)
  }
}