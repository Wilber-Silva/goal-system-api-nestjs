import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupController, GroupManyController } from './group.controller';
import { GroupService } from './group.service';
import { GroupSchema } from './group-schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'groups', schema: GroupSchema }]),
  ],
  controllers: [GroupController, GroupManyController],
  providers: [GroupService],
})
export class GroupModule {}
