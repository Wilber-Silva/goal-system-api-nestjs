import { UpdateResponse } from './../http/update.response';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import { Model } from 'mongoose';
import { Group } from './group';

@Injectable()
export class GroupService {
  constructor(
    @InjectModel('groups') private readonly groupModel: Model<Group>,
  ) {}

  async find (id :Types.ObjectId): Promise<Group> {
    return this.groupModel.findOne(id)
  }

  async list(group: Group): Promise<Group[]> {
    return this.groupModel.find({ ...group });
  }

  async create(group: Group): Promise<Group> {
    return this.groupModel.create(group);
  }

  async update (id: Types.ObjectId, group: Group): Promise<UpdateResponse> {
    return this.groupModel.updateOne(id, group)
  }

  async delete (id: Types.ObjectId): Promise<void> {
    return this.groupModel.updateOne(id, { updatedAt: new Date(), deletedAt: new Date() })
  }

  async createMany(groups: Group[]): Promise<Group[]> {
    return this.groupModel.insertMany(groups)
  }
}
