import { Document } from 'mongoose';

export class Group extends Document {
  name: string;
  tag: string;
}
