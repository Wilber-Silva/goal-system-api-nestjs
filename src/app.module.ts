import { Module } from '@nestjs/common';
import { GoalModule } from './goal/goal.module';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupModule } from './group/group.module';
import { AppController } from './app.controller';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.DATA_BASE),
    GoalModule,
    GroupModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
